from django.contrib import admin
from todos.models import TodoList, TodoItem


@admin.register(TodoList)
class TodoAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]


@admin.register(TodoItem)
class ItemAdmin(admin.ModelAdmin):
    List_display = [
        "task",
        "due_date",
    ]
